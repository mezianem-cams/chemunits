"""Helper functions to handle unit conversions"""
import os
import re
import xarray as xr
import pandas as pd
import numpy as np


from scipy.constants import R, zero_Celsius, atm

_CURRENT_DIR = os.path.dirname(__file__)
PERIODIC_TABLE_PATH = os.path.join(
    _CURRENT_DIR, 'static/PeriodicTableofElements.csv'
)

AIR_MOLAR_MASS = 28.965
PERIODIC_TABLE = pd.read_csv(PERIODIC_TABLE_PATH)

# https://fr.wikipedia.org/wiki/Système_international_d'unités
SI_UNITS = ['kg', 's', 'A', 'K', 'cd', 'mol']
DERIVED_UNITS = SI_UNITS + [
    'rad', 'sr', 'Hz', 'N', 'Pa', 'J', 'W', 'C', 'V',
    'F', 'Ω', 'S', 'Wb', 'T', 'H', 'lm', 'lx', 'Bq', 'Gy',
    'Sv', 'kat'
]

ELEM_UNITS = DERIVED_UNITS.copy()
ELEM_UNITS[0] = 'g'


PREF_TO_FACTORS = {
    'Z': 1e21, 'E': 1e18, 'P': 1e15, 'T': 1e12,
    'G': 1e9, 'M': 1e6, 'k': 1e3, 'h': 1e2, 'da': 1e1,
    'd': 1e-1, 'c': 1e-2, 'm': 1e-3, 'µ': 1e-6, 'u': 1e-6,
    'n': 1e-9, 'p': 1e-12, 'f': 1e-15, 'a': 1e-18
}


def dry_air_density(P, T, celsius=False):
    """
        Get the dry air density at pressure P and temperature T

        Parameters
        ----------

        P : float or array-like

        T : float or array-like

        celsius : boolean
            if True, assume T in °C
            °K otherwise. (default : False)
    """
    if celsius:
        T += zero_Celsius
    return 1e-3 * AIR_MOLAR_MASS * P / (R * T)


STD_DRY_AIR_DENSITY = dry_air_density(atm, 15)


def elements_from_string(species):
    """
        Convert molecular formula to dictionary

        Parameters
        ----------
        species: str
            molecular formula of a given compound

        Returns
        -------
        chem_dict: dict(keys=elem, value=number of elem)
    """
    spec_re = re.compile(r'([A-Z][a-z]?)(\d*)')
    elem_list = np.array(spec_re.findall(species))

    if elem_list.size == 0:
        raise ValueError(f"invalid species : '{species}'")

    elem_list[elem_list == ''] = '1'

    res_dict = {}.fromkeys(elem_list[:, 0], 0)
    for elem_name, num in elem_list:
        res_dict[elem_name] += int(num)

    return res_dict


def molar_mass(chem):
    """
        Gives the molar mass of a compound from
        its molecular formula. (Computed from atomic masses)

        Parameters
        ----------
        chem : str
            molecular formula

        Returns
        -------
        molar_mass : float
            molar mass of 'chem'
    """
    elems = elements_from_string(chem)
    m_tot = 0.0
    for elem, n in elems.items():
        try:
            m_elem, = PERIODIC_TABLE[
                PERIODIC_TABLE['Symbol'] == elem
            ]['AtomicMass']
        except ValueError as e:
            raise ValueError(f"Unknown chemical element : {e}")

        m_tot += m_elem * n

    return m_tot


def pp_suffix_to_fac(suffix):
    if suffix == 'v':
        fac = 1.0
    elif suffix == 'm':
        fac = 1e6
    elif suffix == 'b':
        fac = 1e9
    elif suffix == 't':
        fac = 1e12
    elif suffix == 'q':
        fac = 1e15
    else:
        raise ValueError(f'unknown mixing ratio units : pp{suffix}')
    return fac


def kgkg_to_pp(chem, val_kgkg, suffix='b'):
    """
        Convert values from kg.kg**-1 to pp[q|t|b|m|v]

        Parameters
        ----------
        chem : str

        val_kgkg : float, array-like

        suffix: one of {'b', 't', 'q', 'v' 'm'}
            specifies the unit "part per <x>" where <x> can be:
                * 'q' : quadrillion
                * 't' : trillion
                * 'b' : billion
                * 'm' : million
                * 'v' : volume

        Returns
        -------
         val_pp : float, array-like
    """
    M_chem = molar_mass(chem)
    fac = M_chem / AIR_MOLAR_MASS

    val_pp = pp_suffix_to_fac(suffix) * fac * val_kgkg
    if isinstance(val_pp, xr.DataArray):
        val_pp.attrs = {
            'units': f"pp{suffix}",
            'long_name': f"{chem} mixing ratio",
        }

    return val_pp


def kgkg_to_ugm3(P, T, val_kgkg, celsius=False):
    """
        Convert values in kg.kg-1 to ug.m-3 at a given pressure and temperature

        Parameters
        ----------

        P : float or array-like
            Pressure to use in the conversion

        T : float or array-lke
            Temperature to use in the conversion

        val_kgkg : float or array-like
            values in kg.kg-1

        celsius : boolean
            if True, assume T in °C
            °K otherwise. (default : False)

        Returns
        -------
        val_ugm3 : values in ug.m-3 at (P, T)
    """
    return dry_air_density(P, T, celsius=celsius) * val_kgkg * 1e9


def pp_to_ugm3(chem, P, T, val_pp, suffix='b', celsius=False):
    """
        Convert values in pp(x) to ug.m-3 at a given pressure and temperature

        Parameters
        ----------

        chem : str
            chemical formula of the compound under consideration

        P : float or array-like
            Pressure to use in the conversion

        T : float or array-lke
            Temperature to use in the conversion

        val_pp : float or array-like
            values in pp(x)

        suffix: : str
            suffix for the mixing ratio units
            (default : 'b' for ppb)

        celsius : boolean
            if True, assume T in °C
            °K otherwise. (default : False)

        Returns
        -------
        val_ugm3 : values in ug.m-3 at (P, T)
    """
    pp_fac = 1e9 * pp_suffix_to_fac(suffix)
    conv_fac = (
        pp_fac
        * dry_air_density(P, T, celsius=celsius)
        * molar_mass(chem) / AIR_MOLAR_MASS
    )
    return val_pp * conv_fac


def get_base_unit(units):
    """
        Extract the base unit from a string and transform
        the prefix into a conversion factor.
        Examples :
        1. get_base_unit('km') -> 'm', 1e3
        2. get_base_unit('mA') -> 'A', 1e-3
        3. get_base_unit('cmol') -> 'mol', 1e-2

        Parameters
        ----------

        units : str

        Returns
        -------
        base_unit : str
        conversion : float

    """
    if units in ELEM_UNITS:
        return units

    pref_re = re.compile('^(' + '|'.join(PREF_TO_FACTORS) + ')')
    found = pref_re.search(units)
    if not found:
        raise ValueError(
            "the units '{units}' are not SI or have invalid prefix\n"
            " possible values : " + ' '.join(PREF_TO_FACTORS)
        )

    pref = found.group(0)
    elem_u = units[len(pref):]
    if elem_u not in ELEM_UNITS:
        raise ValueError(
            f"the unit {elem_u} is not SI or derived"
        )
    conv = PREF_TO_FACTORS[pref]
    return elem_u, conv
