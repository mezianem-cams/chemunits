from setuptools import setup, find_packages

desc = 'Utilities to handle units in atmospheric chemistry'

setup(
    name="chemunits",
    packages=find_packages(),
    version='0.0.1',
    author='Mehdi MEZIANE',
    author_email='mehdi.meziane@meteo.fr',
    description=desc
)
