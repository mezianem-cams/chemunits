# `chemunits` : Utilitaires pour gérer les unités en chimie atmosphérique.

`chemunits` est un package python dans lequel je range les fonctions qui me servent
pour faire de la conversion d'unités - typiquement entre rapport de mélange et densité.

On y trouvera aussi des fonctions plus généralement utiles comme par exemple une fonction
de calcul de la masse molaire d'un composé étant donné sa formule brute.

**Attention :** Ce module est encore au stade de **prototype** et doit donc être utilisé avec prudence.


## Installation

Pour installer le module :

1. Clonez le répo dans un repertoire en local: `git clone https://gitlab.com/blabla`
2. Aller dans le repertoire principal du répo et lancer la commande `pip3 --user -e .`

## Exemples d'utilisation

> Ne pas oublier `help(func)`. **Toutes les fonctions du module sont documentées**

```python
# calcul de masse molaire
from chemunits import molar_mass
# convertit les rapports de mélange en pp(x) vers ug/m3
from chemunits import pp_to_ugm3
# decompose une formule brute
from chemunits import elements_from_string


M_O3 = molar_mass('O3')# 47.997
res_ugm3 = pp_to_ugm3(
	'O3', 1013e2, # Pression
	15.,          # Temperature
	1.0,          # Valeur en ppb
	celsius=True
)

res_ugm3 # = 2.029

elements_from_string('CH3Cl')
# Out : {'C': 1, 'H': 3, 'Cl': 1}
```
